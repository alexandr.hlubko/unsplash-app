import {takeLatest, call, put} from 'redux-saga/effects';
import {Actions} from '../Actions';
import {getImages} from '../Api';

function* getImage() {
  try {
    const data = yield call(getImages);
    yield put({type: Actions.FETCH_IMAGES_SUCCESS, images: data});
  } catch (e) {
    yield put({type: Actions.FETCH_IMAGES_FAILURE, error: e.message});
  }
}

export default function* rootSaga() {
  yield takeLatest(Actions.FETCH_IMAGES, getImage);
}
