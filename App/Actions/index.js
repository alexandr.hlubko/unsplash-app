export const Actions = {
  FETCH_IMAGES: 'FETCH_IMAGES',
  FETCH_IMAGES_SUCCESS: 'FETCH_IMAGES_SUCCESS',
  FETCH_IMAGES_FAILURE: 'FETCH_IMAGES_FAILURE',
};

export const fetchImage = () => ({
  type: Actions.FETCH_IMAGES,
});

export const fetchImageSuccess = images => ({
  type: Actions.FETCH_IMAGES_SUCCESS,
  images,
});

export const fetchImageFailure = error => ({
  type: Actions.FETCH_IMAGES_FAILURE,
  error,
});
