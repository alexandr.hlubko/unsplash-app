import {Actions} from '../Actions';

const initialState = {
  list: [],
  error: '',
};

const images = (state = initialState, actions) => {
  switch (actions.type) {
    case Actions.FETCH_IMAGES:
      return state;
    case Actions.FETCH_IMAGES_SUCCESS:
      return {...state, ...{list: actions.images}};
    case Actions.FETCH_IMAGES_FAILURE:
      return {...state, error: actions.error};
    default:
      return state;
  }
};

export default images;
