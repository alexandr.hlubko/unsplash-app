import 'react-native-gesture-handler';
import * as React from 'react';
import {Provider} from 'react-redux';
import Main from './Components/Main';
import ImageView from './Components/ImageView';
import Store from './Store';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Main"
            component={Main}
            options={{title: 'Main page'}}
          />
          <Stack.Screen
            name="ImageView"
            component={ImageView}
            options={{title: 'View image'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
