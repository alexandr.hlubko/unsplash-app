import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

export default props => {
  return (
    <View style={style.container}>
      <Image source={{uri: props.route.params.url}} style={style.image} />
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#404040',
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
});
