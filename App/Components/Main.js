import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  Button,
  TouchableOpacity,
  ScrollView,
  View,
  Image,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from '../Actions';

class Main extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (!this.props.images.length) {
      this.fetchImages();
    }
  }

  fetchImages() {
    this.props.setError('');
    this.props.getImages();
  }

  showToast(message) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      0,
      50,
    );
  }

  render() {
    if (this.props.error) {
      this.showToast('Error loading images');
    }

    return (
      <View style={styles.main}>
        <ScrollView>
          <View style={styles.list}>
            {this.props.error != '' && (
              <Button
                title="Try again"
                onPress={() => {
                  this.fetchImages();
                }}
              />
            )}
            {this.props.images.map((e, i) => (
              <View style={styles.item} key={i}>
                <View style={styles.image}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('ImageView', {
                        url: e.urls.small,
                      });
                    }}>
                    <Image
                      source={{
                        uri: e.urls.small,
                      }}
                      style={{width: 75, height: 75}}
                    />
                  </TouchableOpacity>
                </View>
                <View style={styles.content}>
                  <View>
                    <Text style={styles.title}> {e.alt_description}</Text>
                  </View>
                  <View>
                    <Text>{e.user.name}</Text>
                  </View>
                </View>
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  content: {
    flex: 1,
    paddingRight: 10,
    paddingLeft: 10,
    paddingTop: 5,
  },
  title: {
    fontSize: 10,
    fontWeight: 'bold',
    textShadowOffset: {width: 150, height: 50},
  },
  item: {
    marginBottom: 10,
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    borderColor: '#bfbfbf',
    borderWidth: 1,
  },
  list: {
    padding: 20,
    flex: 1,
  },
  image: {
    width: 75,
    height: 75,
    backgroundColor: 'white',
  },
});

const mapStateToProps = state => ({
  images: state.images.list,
  error: state.images.error,
});

const mapDispatchToProps = dispatch => ({
  getImages: () => dispatch({type: Actions.FETCH_IMAGES}),
  setError: message =>
    dispatch({type: Actions.FETCH_IMAGES_FAILURE, error: message}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
